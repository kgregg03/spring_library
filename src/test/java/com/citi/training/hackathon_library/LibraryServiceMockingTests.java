package com.citi.training.hackathon_library;

import static org.mockito.Mockito.when;

import com.citi.training.hackathon_library.dao.LibraryMongoRepo;
import com.citi.training.hackathon_library.model.Books;
import com.citi.training.hackathon_library.model.ItemType;
import com.citi.training.hackathon_library.model.Library;
import com.citi.training.hackathon_library.service.LibraryService;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class LibraryServiceMockingTests {

    private static final Logger LOG = LoggerFactory.getLogger(LibraryServiceMockingTests.class);

    @Autowired
    private LibraryService libraryService;

    @MockBean
    private LibraryMongoRepo mockLibraryRepo;


    @Test
    public void test_libraryService_save(){

        LOG.debug("This is a message");

        Library testLib = new Library();
        testLib.setItem(ItemType.Books);

        // tell mockRepo that libraryService is going to call save()
        // when it does return the testLib object
        when(mockLibraryRepo.save(testLib)).thenReturn(testLib);

        libraryService.save(testLib);
    } 
    
}
