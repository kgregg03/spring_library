package com.citi.training.hackathon_library.model;

public enum Genre {

    COMEDY, THRILLER, FUNNY, HORROR
    
}
