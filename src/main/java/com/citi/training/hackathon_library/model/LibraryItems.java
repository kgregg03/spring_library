package com.citi.training.hackathon_library.model;

public class LibraryItems {

    private String name;
    private int ISBN;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int iSBN) {
        ISBN = iSBN;
    }

    @Override
    public String toString() {
        return "LibraryItems [ISBN=" + ISBN + ", name=" + name + "]";
    }
    
}
