package com.citi.training.hackathon_library.model;

public class Books extends LibraryItems {

    private String author;
    private Genre genre;
    
    public Books(String author) {
        super();
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Books [author=" + author + "]";
    }


  
    

   

    
    
}
