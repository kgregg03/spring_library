package com.citi.training.hackathon_library.model;

public enum ItemType {

    CDs, DVDs, Books, Periodicals
    
}
