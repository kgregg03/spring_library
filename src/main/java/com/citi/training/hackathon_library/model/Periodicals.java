package com.citi.training.hackathon_library.model;

public class Periodicals extends Books {

    private Genre genre;

    public Periodicals(String author) {
        super(author);
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
    
}
