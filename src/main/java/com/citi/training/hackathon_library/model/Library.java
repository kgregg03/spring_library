package com.citi.training.hackathon_library.model;

public class Library {

    private String id;
    private ItemType item;
    private String name;
    private boolean isBorrowed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ItemType getItem() {
        return item;
    }

    public void setItem(ItemType item) {
        this.item = item;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(boolean isBorrowed) {
        this.isBorrowed = isBorrowed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Library(){

    }
    public Library(String id, ItemType item, String name, boolean isBorrowed) {
        this.id = id;
        this.item = item;
        this.name = name;
        this.isBorrowed = isBorrowed;
    }

    @Override
    public String toString() {
        return "Library [id=" + id + ", isBorrowed=" + isBorrowed + ", item=" + item + ", name=" + name + "]";
    }

    
}
