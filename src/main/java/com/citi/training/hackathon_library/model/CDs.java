package com.citi.training.hackathon_library.model;

public class CDs extends LibraryItems {

    private String artist;

    public CDs(String artist) {
        super();
        this.artist = artist;
    }
    
    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "CDs [artist=" + artist + "]";
    }

    

    

    
    
}
