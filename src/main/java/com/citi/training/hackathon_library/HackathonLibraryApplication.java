package com.citi.training.hackathon_library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackathonLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackathonLibraryApplication.class, args);
	}

}
