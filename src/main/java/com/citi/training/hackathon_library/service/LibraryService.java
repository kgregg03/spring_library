package com.citi.training.hackathon_library.service;

import java.util.List;

import com.citi.training.hackathon_library.dao.LibraryMongoRepo;
import com.citi.training.hackathon_library.model.Library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** 
 * This is the main business logic class for the Library.
 * 
 * @author Katharine
 * @see Employee
*/
@Component
public class LibraryService {

    @Autowired
    private LibraryMongoRepo mongoRepo;

    public List<Library> findAll() {
        return mongoRepo.findAll();
    }

    public Library save(Library item) {
        return mongoRepo.save(item);
    }

    
    
}
