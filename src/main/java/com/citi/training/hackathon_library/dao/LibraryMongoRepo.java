package com.citi.training.hackathon_library.dao;

import com.citi.training.hackathon_library.model.Library;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface LibraryMongoRepo extends MongoRepository<Library, String>{
    
}
